/*
 * 后台接口
 */
let apiHost = 'api/';

// 后台请求路径
let apis = {
    // 测试
    test: 'user/test',
    // 登录
    login: 'user/login',
    // 注册
    register: 'user/register',
    // 登出
    logout: "user/logout",
    // 给邮箱发送验证码
    sendVerification: 'reset/sendByUser',

    // 首页展示项目模块的接口
    // 查询所有
    distributeAll: "itemize/distributeAll",
    // 根据 title 进行查询
    distributeByTitle: "itemize/distributeByTitle",
    // 根据报酬进行查询
    distributeReward: "itemize/distributeByReward",
    // 根据当前时间段进行展示
    distributeTime: "itemize/distributeByAfterTime",
    // 搜索框进行模糊查询
    distributeSeach: "itemize/distributeBySearch",
    // 展示项目的接取人信息
    detailsPageForUser: "itemize/detailsPageForUser",

    // 发布项目模块的接口
    // 获取项目的所有标题
    getTitles: "project/getTitles",
    // 发布项目
    releaseProject: "project/releaseProject",

    //获取邮箱验证码
    sendEmail: "sendEmail",
    //提交用户在发布项目模块输入的数据
    submitdata: "project/releaseProject",

    // 接取项目模块的接口
    // 接取项目
    takeProject: "project/takeProject",

    // 查看我的项目 模块
    // 分类展示我的项目
    distributeMyReceiver: "itemize/distributeMyReceiver",
    // 我发布的项目
    showReleaseProject: "project/showMyReleaseProject",

};

for (let i in apis) {
    apis[i] = apiHost + apis[i]
}

export default apis;