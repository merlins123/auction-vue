import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from "@/views/Login";
import Register from "@/views/Register";
import Change from "@/views/Change";
import Index from '@/views/Index'
import Detail from "@/views/Detail";
import Release from "@/views/Release";
import Account from "@/components/Account";
import ChangePassword from "@/views/ChangePassword";
import ReleasePro from "@/views/ReleasePro";
import ReceivePro from "@/views/ReceivePro";
import NoReceive from "@/components/NoReceive";
import NoSure from "@/components/NoSure";
import NoFinish from "@/components/NoFinish";
import Finish from "@/components/Finish";
import ChangeEmail from "@/views/ChangeEmail";
import Personal from "@/components/Personal";
import News from "@/components/News"

import About from "@/views/About";
import Upload from "@/views/Upload";


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Login
  },
  { // 登录
    path: '/login',
    name: 'Login',
    component: Login
  },
  { // 注册
    path: '/register',
    name: 'Register',
    component: Register
  },
  { // 修改密码
    path: '/change',
    name: 'Change',
    component: Change
  },
  { // 主页
    path: '/index',
    name: 'Index',
    component: Index
  },
  { // 详情
    path: '/detail',
    name: 'Detail',
    component: Detail
  },
  { // 发布项目
    path: '/release',
    name: 'Release',
    component: Release
  },

  { // 关于我
    path: '/about',
    name: 'About',
    component: About,
    children:[
      {
        path: '',
        redirect: '/account'
      },
      { // 关于我->账户安全
        path: '/account',
        name: 'Account',
        component: Account
      },
      { // 关于我->个人信息
        path: '/personal',
        name: 'Personal',
        component: Personal
      },
      { // 关于我->通知消息
        path: '/news',
        name: 'News',
        component: News
      },
    ]
  },

  { //账户安全修改详情页，修改密码密保等
    path: '/changePassword',
    name: 'ChangePassword',
    component: ChangePassword
  },

  { //我的项目->发布的项目
    path: '/releasepro',
    name: 'ReleasePro',
    component: ReleasePro,
    children:[
      {
        path: '',
        redirect: '/noreceive'
      },
      { //我的项目->发布的项目->待接取的项目
        path: '/noreceive',
        name: 'NoReceive',
        component: NoReceive
      },
      { //我的项目->发布的项目->待确认的项目
        path: '/nosure',
        name: 'NoSure',
        component: NoSure
      },
      { //我的项目->发布的项目->待确认完成的项目
        path: '/nofinish',
        name: 'NoFinish',
        component: NoFinish
      },
      { //我的项目->发布的项目->待接取的项目
        path: '/finish',
        name: 'Finish',
        component: Finish
      },
    ]
  },
  {
    path: '/receivepro',
    name: 'ReceivePro',
    component: ReceivePro,
  },
  { // 修改邮箱
    path: '/changeEmail',
    name: 'ChangeEmail',
    component: ChangeEmail
  },

  { // 上传
    path: '/upload',
    name: 'Upload',
    component: Upload
  },
 
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
