import Vue from 'vue'
import Vuex from 'vuex'
import t_project from './module/t_project'
import t_user from './module/t_user'
import t_releasePro from './module/t_releasePro'
import t_detail from './module/t_detail'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  plugins:[createPersistedState()],
  state: {

  },
  mutations: {

  },
  getters: {

  },
  actions: {

  },
  modules: {
    t_project: t_project,
    t_user: t_user,
    t_releasePro: t_releasePro,
    t_detail: t_detail
  }
})
