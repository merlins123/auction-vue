// 详情页数据
const module = {
    namespaced:true,  // 命名空间

    state: {
        detail: {}
    },

    // 设置数据
    mutations: {
        // 设置数据
        SET_DETAIL: (state, detail) => {
            state.detail = detail;
        },

        // 清空数据
        REMOVE_DETAIL: (state) => {
            state.detail = {};
        },

    },

    // 获取数据
    getters: {
        // 获取跳转详情页之前的数据
        getDetailData: (state) => {
            return state.detail;
        },
    },

    //
    actions: {},


}
export default module