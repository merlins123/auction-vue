// 我的项目
const module = {
    namespaced:true,  // 命名空间

    state: {

        //我的项目->发布的项目->待接取的项目
        pendingItems: [],


        //我的项目->发布的项目->挑人的项目
        notConfirmedItemsInfos: [],


        //我的项目->发布的项目->已完成的项目
        completedItems: [],


        //我的项目->发布的项目->进行中的项目
        completionStatusItems: []
    },

    // 设置数据
    mutations: {
        // 我发布了的项目，等待接取
        SET_PENDING: (state, pendingData) => {
            state.pendingItems = pendingData;
        },

        // 项目已被摘取，可以开始挑人
        SET_CONFIRMED: (state, confirmed) => {
            state.notConfirmedItemsInfos = confirmed;
        },

    },

    // 获取数据
    getters: {
        // 获取之前发布的项目
        getPending: (state) => {
            return state.pendingItems;
        },

        // 获取可以挑人的项目
        getConfirmed: (state) => {
            return state.notConfirmedItemsInfos;
        },
    },

    //
    actions: {},


}
export default module