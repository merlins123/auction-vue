// 登录数据
const module = {
    namespaced: true,  // 命名空间
    state: {
        // 存储用户登录之后的数据
        userInfo: {},
    },

    // 设置数据
    mutations: {

        // 保存拿到的用户信息
        SET_USERINFO: (state, userInfo) => {
            state.userInfo = userInfo;
            // 存到 session 中，保证它在刷新的时候不会消失
            sessionStorage.setItem("userInfo", JSON.stringify(userInfo));
        },

        // 清空登录信息
        REMOVE_INFO: state => {
            state.userInfo = '';
            sessionStorage.removeItem("userInfo");
        }
    },

    // 获取数据
    getters: {

        // 获取用户信息
        getUserInfo: state => {
            return state.userInfo;
        }
    },

    actions: {}
}

export default module